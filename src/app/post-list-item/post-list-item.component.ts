import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})

export class PostListItemComponent implements OnInit {

  title : string;
  content : string;
  private loveIts : number;
  created_at : Date;

  constructor(titre:string, contenu : string) {

      this.content = contenu;
      this.title = titre;
      this.loveIts = 0;
      this.created_at = new Date();
   }

  /**
   * Fonction permettant de decrementer le nombre de loveIt
   */
  decrement_loveIt(){
      this.loveIts -= 1;
  }

  /**
   * Fonction permettant d'incrementer le nombre de loveIt
   */
  increment_loveIt(){
      this.loveIts +=1;
  }

  /**
   * Fonction permettant de recuperer le nombre de loveIt 
   */
  getLoveIt(){
    return this.loveIts;
  }

  ngOnInit() {
  }

}
